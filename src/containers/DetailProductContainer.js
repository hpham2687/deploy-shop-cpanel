import React, { useContext } from "react";
import * as ContextApi from "../context";
import MainDetailProduct from "./../components/DetailProductComponents";

const DetailProductContainer = (props) => {
  const idProduct = props.match.params.id;

  const { ProductDataContexts, UiContexts, CartContexts } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);

  return (
    <>
      <MainDetailProduct
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        productDataContext={productDataContext}
        idProduct={idProduct}
        {...props}
      />
    </>
  );
};

export default DetailProductContainer;
