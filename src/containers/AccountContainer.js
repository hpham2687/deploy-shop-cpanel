import React, { useContext } from "react";
import MainAccount from "../components/AccountComponents";
import * as ContextApi from "../context";
import { FirebaseContext } from "../firebase";

const AccountContainer = (props) => {
  const {
    ProductDataContexts,
    UserContexts,
    UiContexts,
    CartContexts,
  } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);
  const userContexts = useContext(UserContexts);
  const firebase = useContext(FirebaseContext);

  return (
    <>
      <MainAccount
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        userContexts={userContexts}
        productDataContext={productDataContext}
        userData={userContexts.user.authUser}
        firebase={firebase}
        {...props}
      />
    </>
  );
};

export default AccountContainer;
