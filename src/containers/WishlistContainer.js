import React, { useContext } from 'react';
import { showNotification } from '../components/commons/Notification';
import MainWishlist from '../components/WishlistComponents';
import { UiContexts, WishlistContexts, CartContexts } from '../context';
import * as uiActions from '../actions/ui';
import * as cartActions from '../actions/cart';

const WishlishContainer = () => {
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);

  const wishlistContext = useContext(WishlistContexts);
  let storedWishList = JSON.parse(localStorage.getItem('wishList')) || [];

  const handleAddToCart = ({ doc_id, doc_data }) => {
    uiContexts.UiDispatch(uiActions.setModalViewCartStatus(true));
    cartContexts.cartDispatch(cartActions.setCart({ doc_id, doc_data }));
    handleDeleteCartItem(doc_id);
  };
  const handleDeleteCartItem = (doc_id) => {
    storedWishList = storedWishList.filter((item) => item.doc_id !== doc_id);
    wishlistContext.numWishlistDispatch({
      type: 'SET_NUM_WISHLIST',
      payload: wishlistContext.numWishlist - 1,
    });

    localStorage.setItem('wishList', JSON.stringify(storedWishList));
  };
  return (
    <>
      <MainWishlist
        storedWishList={storedWishList}
        handleDeleteCartItem={handleDeleteCartItem}
        handleAddToCart={handleAddToCart}
        wishlistContext={wishlistContext}
        uiContexts={uiContexts}
      />
    </>
  );
};

export default WishlishContainer;
