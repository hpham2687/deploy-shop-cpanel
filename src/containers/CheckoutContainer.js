import React, { useContext } from "react";
import MainCheckout from "../components/CheckoutComponents";
import * as ContextApi from "../context";

const CheckoutContainer = () => {
  const { ProductDataContexts, UiContexts, CartContexts } = ContextApi;

  const productDataContext = useContext(ProductDataContexts);
  const uiContexts = useContext(UiContexts);
  const cartContexts = useContext(CartContexts);

  return (
    <>
      <MainCheckout
        uiContexts={uiContexts}
        cartContexts={cartContexts}
        productDataContext={productDataContext}
      />
    </>
  );
};

export default CheckoutContainer;
