export const convert2SmallImg = (url) => {
  if (!url.includes("http")) {
    url = "https://d-themes.com/react/molla/" + url;
  }

  return url;
};

// export const countStarProduct = (reviews, numReview) => {
//   return Math.floor(
//     reviews.reduce(function (acc, val) {
//       return acc + val.count || 2;
//     }, 0) / numReview
//   );
// };

export const filterProduct = (data, filterObject) => {
  const activeTab = filterObject.activeTab;
  const isFilterActiveTab = activeTab !== null;
  return data
    .filter((value) => {
      // lọc qua từng phần tử
      if (!isFilterActiveTab) {
        // nếu có lọc qua active tab,
        return 1;
      }
      if (activeTab === "All") return 1; // nếu tab là all, cho qua
      // check trong từng categrory của phần tử, nếu == với activeTab thì cho qua
      return value.doc_data.category.includes(activeTab);
    })
    .filter((value) => {
      // lọc trendy

      if (!filterObject.filterCategories.includes("trendy")) {
        return 1;
      }
      if (value.doc_data.top) return 1;
      return 0;
    })
    .filter((value) => {
      // lọc theo time
      if (!filterObject.filterCategories.includes("newArrival")) {
        return 1;
      }
      if (!value.doc_data.new) return 1;
      return 0;
    });
};

export const getMaxPriceValue = (data) => {
  let max = 0;
  data.forEach((el) => {
    if (el.doc_data.price >= max) max = el.doc_data.price;
  });
  return max;
};
export const getListCategory = (data) => {
  let array = [];
  data.forEach((el) => {
    const { doc_data } = el;
    let { category } = doc_data;
    category.forEach((el) => array.push(el));
  });

  let uniq = [...new Set(array)];

  let uniqArrayObject = [];
  uniq.forEach((el) =>
    uniqArrayObject.push({
      name: el,
      count: 0,
    })
  );

  // let counts = {};
  // data.forEach(el => counts[el] = 1  + (counts[el] || 0))
  uniqArrayObject.forEach((element) => {
    data.forEach((el) => {
      const { doc_data } = el;
      let { category } = doc_data;
      category.forEach((el) => {
        if (el === element.name) {
          element.count += 1;
        }
      });
    });
  });

  return uniqArrayObject;
};

/**
 * Get trendy products
 * @param {Array} products
 * @return {Array} trendyProducts
 */
export const getTrendyProducts = (products) => {
  return products.filter((item) => item.trendy);
};

/**
 * Get new products
 * @param {Array} products
 * @return {Array} newProducts
 */
export const getNewProducts = (products) => {
  return products.filter((item) => item.new);
};
