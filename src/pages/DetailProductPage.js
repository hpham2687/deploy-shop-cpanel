import React from "react";
import DetailProductContainer from "../containers/DetailProductContainer";

const DetailProductPage = (props) => {
  console.log(props);
  return (
    <>
      <DetailProductContainer {...props} />
    </>
  );
};

export default DetailProductPage;
