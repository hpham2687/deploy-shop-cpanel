import React from 'react';

const HomePage = React.lazy(() => import('./HomePage'));
const AdminPage = React.lazy(() => import('./Admin'));
const CheckOutPage = React.lazy(() => import('./CheckOutPage'));
const DetailProductPage = React.lazy(() => import('./DetailProductPage'));
const AccountPage = React.lazy(() => import('./AccountPage'));
const SidebarListPage = React.lazy(() => import('./SidebarListPage'));
const WishlistPage = React.lazy(() => import('./WishlistPage'));

export {
  HomePage,
  AdminPage,
  CheckOutPage,
  DetailProductPage,
  AccountPage,
  SidebarListPage,
  WishlistPage,
};
