import React from "react";
import SidebarListContainer from "../containers/SidebarListContainer";

const SidebarListPage = (props) => {
  return (
    <>
      <SidebarListContainer {...props} />
    </>
  );
};

export default SidebarListPage;
