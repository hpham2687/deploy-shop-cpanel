import React, { useState } from 'react';
import { convert2SmallImg } from '../../helpers';
import { numberWithCommas } from '../../utils';
import { showNotification } from '../commons/Notification';

const MainWishlish = (props) => {
  const {
    uiContexts,
    wishlistContext,
    handleAddToCart,
    handleDeleteCartItem,
    storedWishList,
  } = props;

  const RenderCartList = () => {
    if (storedWishList.length > 0)
      return storedWishList.map((value, index) => {
        const { doc_id } = value;
        const { doc_data } = value;
        return (
          <tr key={index}>
            <td className="product-col">
              <div className="product">
                <figure className="product-media">
                  <img src={convert2SmallImg(doc_data.pictures[0])} alt="Product" />
                </figure>
                <h3 className="product-title">{doc_data.name}</h3>
              </div>
            </td>
            <td className="price-col">
              {uiContexts.ui.currency === 'usd'
                ? `$${doc_data.price}`
                : `${numberWithCommas(doc_data.price * 22000)} đồng`}
            </td>

            <td style={{ position: 'relative' }} className="total-col">
              <div
                onClick={() => handleAddToCart({ doc_id, doc_data })}
                style={{ left: '50%', top: '50%', transform: 'translate(-50%, -50%)' }}
                className="bot"
              >
                <i style={{ marginRight: '10px' }} className="far fa-cart-plus" /> Add to cart
              </div>
            </td>
            <td
              onClick={(e) => {
                handleDeleteCartItem(doc_id);
                showNotification('success', 'Item removed from your wishlist!');
              }}
              className="remove-col"
            >
              <i className="fal fa-times" />
            </td>
          </tr>
        );
      });
    return (
      <tr style={{ border: 'none' }}>
        <td>No Products in Cart!</td>
        <td />
        <td />
        <td />
        <td />
      </tr>
    );
  };
  return (
    <div className="checkout-containter">
      <div className="page-title-banner">
        <h3 className="page-title-banner___title">Wishlist</h3>
        <span className="page-title-banner___subtitle">SHOP</span>
      </div>

      <div className="checkout-content-wrapper">
        <div className="grid wide">
          <div className="row">
            <table className="table-cart">
              <thead>
                <tr>
                  <th>Product</th>
                  <th>Price</th>
                  <th>Action</th>
                  <th />
                </tr>
              </thead>
              <tbody>
                <RenderCartList />
              </tbody>
            </table>
          </div>
        </div>
      </div>
    </div>
  );
};

export default MainWishlish;
