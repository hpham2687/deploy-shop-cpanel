import React, { useContext, useEffect, useState } from 'react';
import $ from 'jquery';
import { Link } from 'react-router-dom';
import { useTranslation } from 'react-i18next';
import Fade from 'react-reveal/Fade';
import { convert2SmallImg } from '../../helpers';
import { showNotification } from './Notification';
import * as uiActions from '../../actions/ui';
import { numberWithCommas, countWishlish } from '../../utils';

const Header = (props) => {
  const { t, i18n } = useTranslation();
  const { uiContexts, cartContexts, userContexts, firebase, wishlistContext } = props;
  const { authUser } = userContexts.user;
  const menuMobileRef = React.createRef();
  const [language, setLanguage] = useState('eng');
  const currentInCart =
    cartContexts.cart.reduce(function (acc, val) {
      return acc + val.count;
    }, 0) || 0;

  const listDropdownItems = cartContexts.cart;

  const RenderListDropdownItems = () => {
    if (listDropdownItems.length > 0)
      return listDropdownItems.map((value, index) => {
        const { count, doc_data, doc_id } = value;
        return (
          <li key={index}>
            <div className="dropdown-cart-items-left">
              <h3>{doc_data.name}</h3>
              <span className="dropdown-cart-items-left__amount">
                {count}x
                {uiContexts.ui.currency === 'usd'
                  ? `$${doc_data.price}`
                  : `${numberWithCommas(doc_data.price * 22000)} đồng`}
              </span>
            </div>
            <div className="dropdown-cart-items-right">
              <img src={convert2SmallImg(doc_data.pictures[0])} />
              <span
                onClick={() =>
                  cartContexts.cartDispatch({
                    type: 'DELETE',
                    payload: doc_id,
                  })
                }
                className="dropdown-cart-items-left__close"
              >
                X
              </span>
            </div>
          </li>
        );
      });
    return <>{t('header.no-product-added')}</>;
  };

  const changeLanguage = (lng) => {
    i18n.changeLanguage(lng);
  };

  //  console.log(authUser)
  const handleLogOut = () => {
    firebase
      .doSignOut()
      .then(() => {
        showNotification('success', 'Log out successfully!');
      })
      .catch((error) => {
        showNotification('success', `Log out error!${error}`);
      });
  };
  const closeSidebarMobile = () => {
    //  alert('vao day')
    // $('.toggle-menu-mobile')[0].checked = false
  };

  const handleClickLogin = () => {
    closeSidebarMobile();
    if (!authUser) uiContexts.UiDispatch(uiActions.setModalAuthStatus(true));
    else {
      // click account
      uiContexts.UiDispatch(uiActions.setModalwModalSidebar(true));
      // click account
      $('.modal-sidebar').removeClass('modal-sidebar--hidden');
      $('.modal-sidebar-wrap').css('opacity', '1');
      $('.modal-sidebar-wrap').css('visibility', 'visible');
    }
  };
  useEffect(() => {
    $('.menu-li > li')
      .slice(1)
      .hover(
        function () {
          $('.shop-menu-dropdown').css('display', 'flex');
        },
        function () {
          $('.shop-menu-dropdown').css('display', 'none');
        }
      );

    $('#view-cart-icon').hover(
      function () {
        $('.dropdown-cart-items').css('display', 'block');
      },
      function () {
        $('.dropdown-cart-items').css('display', 'none');
      }
    );
  }, []);
  return (
    <>
      <header className="header">
        <div className="grid wide">
          <div className="header__top">
            <div className="header__top-left">
              <div className="dropdown-menu">
                <a href="#">
                  {uiContexts.ui.currency === 'usd' ? 'USD' : 'VND'}
                  <i className="fa fa-angle-down" />
                </a>
                <div className="dropdown-menu__list">
                  <ul>
                    <li>
                      <a
                        onClick={(e) => {
                          uiContexts.UiDispatch(uiActions.setCurrency('vnd'));
                          showNotification('success', 'Currency is changed to VND successfully!');
                        }}
                        href="#"
                      >
                        Vnd
                      </a>
                    </li>
                    <li>
                      <a
                        onClick={(e) => {
                          uiContexts.UiDispatch(uiActions.setCurrency('usd'));
                          showNotification('success', 'Currency is changed to USD successfully!');
                        }}
                        href="#"
                      >
                        Usd
                      </a>
                    </li>
                  </ul>
                </div>
              </div>

              <div className="dropdown-menu">
                <a href="#">
                  {language === 'eng' ? 'ENG' : 'VIE'}
                  <i className="fa fa-angle-down" />
                </a>
                <div className="dropdown-menu__list">
                  <ul>
                    <li
                      onClick={() => {
                        changeLanguage('vi');
                        setLanguage('vie');
                        showNotification('success', 'Language is changed to VIE successfully!');
                      }}
                    >
                      <a href="#">VIE</a>
                    </li>
                    <li
                      onClick={() => {
                        changeLanguage('en');
                        setLanguage('eng');
                        showNotification('success', 'Language is changed to ENG successfully!');
                      }}
                    >
                      <a href="#">ENG</a>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div className="header__top-right">
              <ul>
                <li>
                  <i className="fa fa-phone" />
                  CALL: +0123 456 789
                </li>
                <li>
                  <Link to="/wishlist">
                    <i className="far fa-heart" />
                    MY WISHLIST ({wishlistContext.numWishlist})
                  </Link>
                </li>
                <li onClick={() => handleClickLogin()}>
                  <i className="fa fa-user" />
                  {authUser ? 'ACCOUNT' : 'LOGIN'}
                </li>

                {authUser ? <li onClick={() => handleLogOut()}>LOG OUT</li> : null}
              </ul>
            </div>
          </div>
          <div className="header__bottom">
            <div className="header__bottom-left">
              <label
                onClick={() => {
                  const menuMobile = menuMobileRef.current;
                  menuMobile.classList.toggle('isOpen');
                }}
                htmlFor="toggle-menu-mobile"
              >
                <div className="bar-mobile moblie">
                  <i className="fa fa-bars" />
                </div>
              </label>
              <input
                type="checkbox"
                id="toggle-menu-mobile"
                defaultChecked="false"
                className="toggle-menu-mobile"
              />

              <div ref={menuMobileRef} className="menu-mobile">
                <label
                  onClick={() => {
                    const menuMobile = menuMobileRef.current;
                    menuMobile.classList.toggle('isOpen');
                  }}
                  htmlFor="toggle-menu-mobile"
                >
                  <span className="menu-mobile__close">
                    <i className="fas fa-times" />
                  </span>
                </label>
                <ul className="list-sidebar-menu">
                  {authUser ? (
                    <>
                      <li onClick={() => closeSidebarMobile()}>
                        <Link to="/account">
                          <label
                            onClick={() => {
                              const menuMobile = menuMobileRef.current;
                              menuMobile.classList.toggle('isOpen');
                            }}
                            htmlFor="toggle-menu-mobile"
                          >
                            My Account
                          </label>
                        </Link>
                      </li>
                      <li>my wish list</li>
                      <li onClick={() => handleLogOut()}>sign out</li>
                      <li>welcome, hung pham</li>{' '}
                    </>
                  ) : (
                    <>
                      <li>welcome, guest</li>
                      <li onClick={() => handleClickLogin()}>
                        <span>Login</span>
                      </li>
                      <li>Products</li>
                    </>
                  )}
                </ul>
              </div>
              <div className="logo">
                <Link to="/">
                  <img
                    src="https://d-themes.com/react/molla/demo-2/assets/images/logo.png"
                    width="105"
                    height="25"
                    alt="logo image"
                  />
                </Link>
              </div>
              <ul className="menu-li">
                <li className="active-li">
                  <Link style={{ color: '#000' }} to="/">
                    {t('header.menu-li.home')}
                  </Link>
                </li>
                <li>
                  SHOP
                  <i className="fa fa-angle-down" />
                  <div className="shop-menu-dropdown">
                    <div className="wrapper12">
                      <div className="col1">
                        <div className="menu-dropdown__title">SHOP WITH SIDEBAR</div>
                        <ul>
                          <li>Shop List</li>
                          <li>Shop Grid 2 Columns</li>
                          <li>Shop Grid 3 Columns</li>
                          <li>Shop Grid 4 Columns</li>
                        </ul>
                        <div className="menu-dropdown__title">SHOP NO SIDEBAR</div>
                        <ul>
                          <li>Shop Boxed No Sidebar</li>
                          <li>Shop Fullwidth No Sidebar</li>
                        </ul>
                      </div>
                      <div className="col2">
                        <div className="menu-dropdown__title">SHOP WITH SIDEBAR</div>
                        <ul>
                          <li>Shop List</li>
                          <li>Shop Grid 2 Columns</li>
                          <li>Shop Grid 3 Columns</li>
                          <li>Shop Grid 4 Columns</li>
                        </ul>
                        <div className="menu-dropdown__title">SHOP NO SIDEBAR</div>
                        <ul>
                          <li>Shop Boxed No Sidebar</li>
                          <li>Shop Fullwidth No Sidebar</li>
                        </ul>
                      </div>
                    </div>
                    <div className="col3">
                      <img
                        src="https://d-themes.com/react/molla/demo-2/assets/images/menu/banner-1.jpg"
                        alt="Banner"
                        style={{ objectFit: 'contain' }}
                      />
                    </div>
                  </div>
                </li>
                <li>
                  {t('header.menu-li.product')}

                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  {t('header.menu-li.pages')}

                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  {t('header.menu-li.shop')}

                  <i className="fa fa-angle-down" />
                </li>
                <li>
                  {t('header.menu-li.elements')}
                  <i className="fa fa-angle-down" />
                </li>
              </ul>
            </div>
            <div className="header__bottom-right">
              <ul>
                <li className="li-search-group">
                  <label htmlFor="check-search" className="check-search">
                    <i className="far fa-search" />
                  </label>
                  <input type="checkbox" id="check-search" name="check-search" />
                  <input
                    type="search"
                    className="input-search"
                    name="q"
                    id="q"
                    placeholder={t('header.search')}
                    required
                  />
                </li>
                <li id="view-cart-icon">
                  <Link to="/checkout">
                    <i className="far fa-shopping-cart" />
                    <span className="cart-number">{currentInCart}</span>
                  </Link>
                  <Fade distance="10%" duration={600} top>
                    <div className="dropdown-cart-items">
                      <div className="dropdown-cart-items__container">
                        <ul>
                          <RenderListDropdownItems />
                        </ul>
                        <Link to="/checkout">
                          <div className="bot">{t('header.go-to-checkout-btn')}</div>
                        </Link>
                      </div>
                    </div>
                  </Fade>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </header>
    </>
  );
};

export default Header;
