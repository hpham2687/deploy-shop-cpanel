import React, { useContext, useEffect } from 'react';
import { Redirect, Route } from 'react-router-dom';
import * as ContextApi from '../../context';
import { showNotification } from './Notification';

const PrivateRoute = ({ component: Component, ...rest }) => {
  const { UserContexts } = ContextApi;

  const userContexts = useContext(UserContexts);
  const userData = userContexts.user.authUser;

  useEffect(() => {
    if (!userData) showNotification('success', 'Loggin Firstss!');
  }, []);
  return (
    <Route
      {...rest}
      render={(props) => (userData ? <Component {...props} /> : <Redirect to="/" />)}
    />
  );
};

export default PrivateRoute;
