import React, { useContext, Component, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { CSSTransition } from 'react-transition-group';
import PropTypes from 'prop-types'; // ES6
import { Formik } from 'formik';
import { Form, Field, ErrorMessage } from 'formik';

import Fade from 'react-reveal/Fade';
import { showNotification } from '../commons/Notification';
import { FirebaseContext } from '../../firebase';
import * as uiActions from '../../actions/ui';
import loadingIcon from '../../assets/img/loading-auth-logo.gif';

function ModalLogin(props) {
  const firebase = useContext(FirebaseContext);
  const { uiContexts } = props;

  const [showMessage, setShowMessage] = useState(false);
  useEffect(() => {
    if (uiContexts.ui.isShowModalAuth) setShowMessage(true);
    else setShowMessage(false);
  }, [uiContexts]);

  return (
    <>
      <CSSTransition in={uiContexts.ui.isShowModalAuth} classNames="alert" timeout={300}>
        <SignInForm firebase={firebase} {...props} />
      </CSSTransition>
      <CSSTransition in={uiContexts.ui.isShowModalAuth} classNames="my-node" timeout={300}>
        <div className="modal-molla__overlay" />
      </CSSTransition>
    </>
  );
}

ModalLogin.propTypes = {
  userContexts: PropTypes.shape({
    user: PropTypes.object,
    userDispatch: PropTypes.func,
  }),
  // uiContexts: PropTypes.oneOfType([PropTypes.arrayOf(PropTypes.node), PropTypes.node]),
};

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
  loading: false,
  currentTab: 'login',
};
class SignInFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.firebase = { ...this.props.firebase };
    this.userContexts = { ...this.props.userContexts };
    this.uiContexts = { ...this.props.uiContexts };

    this.state = {
      currentTab: 'login',
      isShowing: false,
    };
  }

  handleAddUserToDb = (document2Add) => {
    console.log(document2Add);
    this.firebase
      .addDocument('usersData', document2Add)
      .then(function (docRef) {
        console.log('userData <- Document written with ID: ', docRef.id);
      })
      .catch(function (error) {
        console.error('Error adding document: ', error);
      });
  };

  handleSubmitRegister = ({ email, password }) => {
    this.setState({ loading: true });

    this.firebase
      .createUserWithEmailAndPassword(email, password)
      .then((authUser) => {
        // console.log(authUser);
        showNotification('success', 'Register successfully!');
        // add user data to firestore
        this.handleAddUserToDb({
          uid: authUser.user.uid,
          displayName: authUser.user.displayName,
          email: authUser.user.email,
          phoneNumber: authUser.user.phoneNumber,
          photoURL: authUser.user.photoURL,
        });
        this.handleCloseModal();
      })
      .catch((error) => {
        this.setState({ error });
        showNotification('success', `Register error!${error}`);

        // this.props.history.push('/error');
      });
    this.setState({ loading: false });
  };

  handleSubmitLogin = ({ email, password }) => {
    this.setState({ loading: true });

    this.firebase
      .signInWithEmailAndPassword(email, password)
      .then(async (authUser) => {
        const refresh_token = authUser.user.refreshToken;
        const id_token = authUser.user.xa;
        localStorage.setItem('refresh_token', refresh_token);
        localStorage.setItem('id_token', id_token);

        showNotification('success', 'Login successfully!');
        this.handleCloseModal();
      })
      .catch((error) => {
        this.setState({ error });
        showNotification('success', `Login error!${error}`);

        this.setState({ loading: false });
      });
  };

  onChange = (event) => {
    this.setState({ [event.target.name]: event.target.value });
  };

  handleClickTab = (event) => {
    const nameTabClicked = event.currentTarget.dataset.name;
    if (nameTabClicked === 'loginTab') {
      this.setState({ currentTab: 'login' });
    } else if (nameTabClicked === 'registerTab') {
      this.setState({ currentTab: 'register' });
    }
  };

  handleCloseModal = () => {
    this.uiContexts.UiDispatch(uiActions.setModalAuthStatus(false));
  };

  RenderLoginTab = () => {
    const { loading } = this.state;

    return (
      <>
        <Formik
          enableReinitialize
          initialValues={{ email: 'default', password: '' }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = 'Required';
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
              errors.email = 'Invalid email address';
            }
            if (!values.password) {
              errors.password = 'Required';
            } else if (values.password.length < 6) {
              errors.password = 'Must be 6 characters or more';
            }

            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            this.handleSubmitLogin(values);
            setSubmitting(false);
          }}
        >
          {({ values, errors, touched, handleChange, handleBlur, handleSubmit, isSubmitting }) => (
            <form onSubmit={handleSubmit}>
              <Fade>
                <div className="form-group">
                  <div className="label-input">Username or email address *</div>
                  <input
                    type="email"
                    name="email"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.email}
                    className={`form-control-input ${
                      touched && errors.email ? '--input-error' : null
                    }`}
                  />
                  <small className="form-text">
                    {errors.email && touched.email && errors.email}
                  </small>
                </div>
                <div className="form-group">
                  <div className="label-input">Password *</div>
                  <input
                    type="password"
                    name="password"
                    onChange={handleChange}
                    onBlur={handleBlur}
                    value={values.password}
                    className={`form-control-input ${
                      touched && errors.password ? '--input-error' : null
                    }`}
                  />
                  <small className="form-text">
                    {errors.password && touched.password && errors.password}
                  </small>
                </div>
                <div className="login-row">
                  <button
                    style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                    disabled={isSubmitting}
                    type="submit"
                    className="btn-outline-orange"
                  >
                    SHOP NOW
                    {loading ? (
                      <img
                        style={{ display: 'inline-block', height: '28px', marginLeft: '10px' }}
                        src={loadingIcon}
                      />
                    ) : (
                      <i className="fa fa-caret-right" />
                    )}
                  </button>
                  <div
                    onClick={() => {
                      this.uiContexts.UiDispatch(uiActions.setModalAuthStatus(false));

                      this.uiContexts.UiDispatch(uiActions.setModalResetPassword(true));
                    }}
                    className="forgot-password"
                  >
                    Forgot Your Password?
                  </div>
                </div>
              </Fade>
            </form>
          )}
        </Formik>
      </>
    );
  };

  RenderRegisterTab = () => {
    const { loading } = this.state;

    return (
      <>
        <Formik
          initialValues={{ email: '', password: '', checkbox: [] }}
          validate={(values) => {
            const errors = {};
            if (!values.email) {
              errors.email = 'Required';
            } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
              errors.email = 'Invalid email address';
            }
            if (!values.password) {
              errors.password = 'Required';
            } else if (values.password.length < 6) {
              errors.password = 'Must be 6 characters or more';
            }
            if (values.checkbox.length < 1) {
              errors.checkbox = 'Required';
            }
            return errors;
          }}
          onSubmit={(values, { setSubmitting }) => {
            //  console.log(values.checkbox);

            this.handleSubmitRegister(values);
            setSubmitting(false);
          }}
        >
          {({ values, errors, handleChange, touched, handleBlur, handleSubmit, isSubmitting }) => (
            <>
              <form onSubmit={handleSubmit}>
                <Fade>
                  <div className="form-group">
                    <div className="label-input">Your email address *</div>
                    <input
                      type="email"
                      name="email"
                      onBlur={handleBlur}
                      value={values.email}
                      onChange={handleChange}
                      className={`form-control-input ${
                        touched && errors.email ? '--input-error' : null
                      }`}
                    />
                    <small className="form-text">
                      <ErrorMessage name="email" component="div" />
                    </small>
                  </div>
                  <div className="form-group">
                    <div className="label-input">Password *</div>
                    <input
                      type="password"
                      name="password"
                      onBlur={handleBlur}
                      value={values.password}
                      onChange={handleChange}
                      className={`form-control-input ${
                        touched && errors.password ? '--input-error' : null
                      }`}
                    />
                    <small className="form-text">
                      <ErrorMessage name="password" component="div" />
                    </small>
                  </div>
                  <div className="login-row">
                    <button type="submit" disabled={isSubmitting} className="btn-outline-orange">
                      SIGN UP
                      {loading ? (
                        <img
                          style={{ display: 'inline-block', height: '28px', marginLeft: '10px' }}
                          src={loadingIcon}
                        />
                      ) : (
                        <i className="fa fa-caret-right" />
                      )}
                    </button>
                    <div className="remember-me">
                      <Field type="checkbox" name="checkbox" id="checkbox" value="agree" />
                      <label htmlFor="signin-remember">I agree to the privacy policy *</label>
                      <small className="form-text">
                        <ErrorMessage name="checkbox" component="div" />
                      </small>
                    </div>
                  </div>
                </Fade>
              </form>
            </>
          )}
        </Formik>
      </>
    );
  };

  render() {
    const { error, currentTab } = this.state;

    const RenderForm = currentTab === 'login' ? this.RenderLoginTab : this.RenderRegisterTab;

    return (
      <>
        <div className="modal-auth">
          <div className="modal-auth__container">
            <div onClick={() => this.handleCloseModal()} className="modal-auth__close-btn">
              <i className="fal fa-times" />
            </div>
            <div className="modal-auth__tab">
              <span
                data-name="loginTab"
                onClick={(e) => this.handleClickTab(e)}
                className={currentTab === 'login' ? 'modal-auth__tab--active' : null}
              >
                Sign In
              </span>
              <span
                data-name="registerTab"
                onClick={(e) => this.handleClickTab(e)}
                className={currentTab === 'register' ? 'modal-auth__tab--active' : null}
              >
                Register
              </span>
            </div>
            <RenderForm />
            {error && error.message}
          </div>
        </div>
      </>
    );
  }
}

const SignInForm = compose(withRouter)(SignInFormBase);

function areEqual(prevProps, nextProps) {
  if (prevProps.uiContexts.ui.isShowModalAuth === nextProps.uiContexts.ui.isShowModalAuth)
    return true;
  return false;
  /* Trả về true nếu nextProps bằng prevProps, ngược lại trả về false */
}

export default React.memo(ModalLogin, areEqual);
