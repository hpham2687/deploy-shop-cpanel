import React, { useContext, Component, useState, useEffect } from 'react';
import { withRouter } from 'react-router-dom';
import { compose } from 'recompose';
import { CSSTransition } from 'react-transition-group';
import { Formik } from 'formik';
import { FirebaseContext } from '../../firebase';
import * as uiActions from '../../actions/ui';

function ModalResetPassword(props) {
  const firebase = useContext(FirebaseContext);
  const { uiContexts } = props;

  const [showMessage, setShowMessage] = useState(false);
  useEffect(() => {
    if (uiContexts.ui.isShowModalResetPassword) setShowMessage(true);
    else setShowMessage(false);
  }, [uiContexts]);

  return (
    <>
      <CSSTransition in={uiContexts.ui.isShowModalResetPassword} classNames="alert" timeout={300}>
        <ResetForm firebase={firebase} {...props} />
      </CSSTransition>
      <CSSTransition in={uiContexts.ui.isShowModalResetPassword} classNames="my-node" timeout={300}>
        <div className="modal-molla__overlay" />
      </CSSTransition>
    </>
  );
}

const INITIAL_STATE = {
  email: '',
  password: '',
  error: null,
  loading: false,
  currentTab: 'login',
};
class ResetFormBase extends Component {
  constructor(props) {
    super(props);

    this.state = { ...INITIAL_STATE };
    this.firebase = { ...this.props.firebase };
    this.userContexts = { ...this.props.userContexts };
    this.uiContexts = { ...this.props.uiContexts };

    this.state = {
      currentTab: 'login',
      isShowing: false,
    };
  }

  handleCloseModal = () => {
    this.uiContexts.UiDispatch(uiActions.setModalResetPassword(false));
  };

  handleSubmit = ({ email, password }) => {};

  render() {
    const { error, currentTab } = this.state;

    return (
      <>
        <div className="modal-auth">
          <div className="modal-auth__container">
            <div onClick={() => this.handleCloseModal()} className="modal-auth__close-btn">
              <i className="fal fa-times" />
            </div>
            <div className="modal-auth__tab">
              <span
                data-name="loginTab"
                className={currentTab === 'login' ? 'modal-auth__tab--active' : null}
              >
                Reset Password
              </span>
            </div>

            <Formik
              initialValues={{ email: '' }}
              validate={(values) => {
                const errors = {};
                if (!values.email) {
                  errors.email = 'Required';
                } else if (!/^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i.test(values.email)) {
                  errors.email = 'Invalid email address';
                }
                return errors;
              }}
              onSubmit={(values, { setSubmitting }) => {
                const actionCodeSettings = {
                  url: `http://localhost:3000/?email=${values.email}`,
                };
                this.firebase
                  .doPasswordReset(values.email, actionCodeSettings)
                  .then(function () {
                    // Verification email sent.
                    console.log('mail sent');
                  })
                  .catch(function (error) {
                    // Error occurred. Inspect error.code.
                    console.log(error);
                  });

                setSubmitting(false);
              }}
            >
              {({
                values,
                errors,
                touched,
                handleChange,
                handleBlur,
                handleSubmit,
                isSubmitting,
                /* and other goodies */
              }) => (
                <form onSubmit={handleSubmit}>
                  <div className="form-group">
                    <div className="label-input">Email *</div>
                    <input
                      type="email"
                      name="email"
                      className="form-control-input"
                      onChange={handleChange}
                      onBlur={handleBlur}
                      value={values.email}
                    />
                    <small className="form-text">
                      {errors.email && touched.email && errors.email}
                    </small>
                  </div>
                  <div className="login-row">
                    <button
                      style={{ display: 'flex', justifyContent: 'center', alignItems: 'center' }}
                      type="submit"
                      className="btn-outline-orange"
                    >
                      Reset Password
                    </button>
                  </div>
                </form>
              )}
            </Formik>

            {error && error.message}
          </div>
        </div>
      </>
    );
  }
}

const ResetForm = compose(withRouter)(ResetFormBase);

export default React.memo(ModalResetPassword);
