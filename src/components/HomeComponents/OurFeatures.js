import React from "react";
import { useTranslation } from "react-i18next";

const OurFeatures = () => {
  const { t, i18n } = useTranslation();

  return (
    <>
      <div className="our-features">
        <div className="grid wide">
          <div className="row">
            <div className="col l-4 m-4 c-12">
              <div className="our-features-item">
                <h3 className="our-features-item__title">
                  <i className="far fa-rocket" />
                  <br />
                  {t("below_arrival.payment_title")}
                </h3>
                <span className="our-features-item__description">
                  {t("below_arrival.payment_content")}
                </span>
              </div>
            </div>
            <div className="col l-4 m-4 c-12">
              <div className="our-features-item">
                <h3 className="our-features-item__title">
                  <i className="far fa-undo" />
                  <br />
                  {t("below_arrival.return_title")}
                </h3>
                <span className="our-features-item__description">
                  {t("below_arrival.return_content")}
                </span>
              </div>
            </div>
            <div className="col l-4 m-4 c-12">
              <div className="our-features-item">
                <h3 className="our-features-item__title">
                  <i className="far fa-life-ring" />
                  <br />
                  {t("below_arrival.customer_title")}
                </h3>
                <span className="our-features-item__description">
                  {t("below_arrival.customer_content")}
                </span>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default OurFeatures;
