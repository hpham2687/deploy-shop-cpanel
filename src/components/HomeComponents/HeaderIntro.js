import React, { Fragment } from 'react';
import * as Scroll from 'react-scroll';
import { Link as LinkRoute } from 'react-router-dom';
import Slider from 'react-slick';

const { Link } = Scroll;

const settings = {
  dots: true,
  infinite: true,
  autoplaySpeed: 3000,
  slidesToShow: 1,
  slidesToScroll: 1,
  autoplay: true,
};

const HeaderIntro = () => {
  const renderBrandAds = [1, 2, 3, 4, 5, 6].map((value) => (
    <Fragment key={value}>
      <li>
        <img src={`https://d-themes.com/react/molla/assets/images/brands/${value}.png`} alt="" />
      </li>
    </Fragment>
  ));
  return (
    <>
      <div className="intro">
        <div className="grid wide">
          <div className="row">
            <div id="wrap-slider" className="col l-8 m-12 c-12">
              <Slider className="slide-header" id="slide-header" {...settings}>
                <div>
                  <div className="carousel__viewport">
                    <div className="carousel__slide slide2">
                      <div className="carousel__text">
                        <div className="carousel__title">
                          <h3 className="subtitle">Outdoor Furniture</h3>
                          <h1 className="main-title">
                            Outdoor Dining <br /> Furniture
                          </h1>
                        </div>
                        <div className="carousel__btn btn-outline-white">
                          <LinkRoute
                            style={{ textDecoration: 'none', color: 'white' }}
                            to="/sidebar-list"
                          >
                            SHOP NOW
                            <i className="fa fa-caret-right" />
                          </LinkRoute>
                        </div>
                      </div>
                      <div className="intro-overlay" />
                    </div>
                  </div>
                </div>
                <div>
                  <div className="carousel__viewport">
                    <div className="carousel__slide slide1">
                      <div className="carousel__text">
                        <div className="carousel__title">
                          <h3 className="subtitle">Outdoor Furniture</h3>
                          <h1 className="main-title">
                            Outdoor Dining <br /> Furniture
                          </h1>
                        </div>
                        <Link to="myScrollToElement" smooth>
                          <div className="carousel__btn btn-outline-white">
                            <LinkRoute
                              style={{ textDecoration: 'none', color: 'white' }}
                              to="/sidebar-list"
                            >
                              SHOP NOW
                              <i className="fa fa-caret-right" />
                            </LinkRoute>
                          </div>
                        </Link>
                      </div>
                      <div className="intro-overlay" />
                    </div>
                  </div>
                </div>
                <div>
                  <div className="carousel__viewport">
                    <div className="carousel__slide slide3">
                      <div className="carousel__text">
                        <div className="carousel__title">
                          <h3 className="subtitle">Outdoor Furniture</h3>
                          <h1 className="main-title">
                            Outdoor Dining <br /> Furniture
                          </h1>
                        </div>
                        <div className="carousel__btn btn-outline-white">
                          <LinkRoute
                            style={{ textDecoration: 'none', color: 'white' }}
                            to="/sidebar-list"
                          >
                            SHOP NOW
                          </LinkRoute>
                          <i className="fa fa-caret-right" />
                        </div>
                      </div>
                      <div className="intro-overlay" />
                    </div>
                  </div>
                </div>
              </Slider>
            </div>
            <div className="flex-intro-23 col l-4 m-0 c-12">
              <div className="intro-2">
                <div className="intro-overlay" />
                <div className="intro-2__text">
                  <div className="intro-2__title">
                    <h3 className="subtitle">Outdoor Furniture</h3>
                    <h1 className="main-title">
                      Outdoor Dining <br /> Furniture
                    </h1>
                  </div>
                  <div className="carousel__btn btn-outline-white intro-2-btn">
                    <Link to="myScrollToShopByCategory" smooth>
                      SHOP NOW
                    </Link>
                    <i className="fa fa-caret-right" />
                  </div>
                </div>
              </div>
              <div className="intro-3">
                <div className="intro-overlay" />
                <div className="intro-2__text">
                  <div className="intro-2__title">
                    <h3 className="subtitle">New in</h3>
                    <h1 className="main-title">
                      Best Lighting <br /> Collection
                    </h1>
                  </div>
                  <div className="carousel__btn btn-outline-white intro-2-btn">
                    <Link to="myScrollToTrendyProduct" smooth>
                      Discover Now
                    </Link>

                    <i className="fa fa-caret-right" />
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col l-12 m-6 c-12">
              <div className="brand-ads">
                <ul>{renderBrandAds}</ul>
              </div>
            </div>
          </div>
        </div>
      </div>
    </>
  );
};

export default HeaderIntro;
