import React, { useEffect, useState } from 'react';

import Slider from 'react-slick';
import 'slick-carousel/slick/slick.css';
import 'slick-carousel/slick/slick-theme.css';

import { CSSTransition } from 'react-transition-group';
// ES6 Imports
import * as Scroll from 'react-scroll';
import { useTranslation } from 'react-i18next';
import { filterProduct, isInWishlish } from '../../utils';
import Product from '../Product';
import Spinner from '../commons/Spinner';

const { Element } = Scroll;

const TrendyProduct = (props) => {
  const [showing, setShowing] = useState(false);

  const { t } = useTranslation();
  const {
    uiContexts,
    productDataContext,
    productDataContext: { productData },
    handleAddToWishLists,
    handleAddToCart,
    handleClickTab,
  } = props;

  const activeTab = uiContexts.ui.trendyProductTab;

  useEffect(() => {
    setShowing(true);
    const intervalId = setInterval(() => {
      setShowing(false);
    }, 1000);
    return () => {
      clearInterval(intervalId);
    };
  }, [activeTab]);

  const RenderStar = ({ numStar }) => {
    return [...Array(5)].map((value, index) => (
      <i key={index} className={`fa fa-star ${index + 1 <= numStar ? 'active-star' : null}`} />
    ));
  };

  const ListTrendyProducts = () => {
    if (productData.length > 0)
      return filterProduct(productData, {
        activeTab,
        filterCategories: [''],
      })
        .sort((a, b) => parseFloat(b.price) > parseFloat(a.price))
        .map((value) => {
          const { doc_id, doc_data } = value;
          return (
            <Product
              isInWishlish={isInWishlish(doc_id)}
              length={productData.length}
              key={doc_id}
              doc_id={doc_id}
              doc_data={doc_data}
              handleAddToWishList={handleAddToWishLists}
              handleAddToCart={handleAddToCart}
              uiContexts={uiContexts}
              RenderStar={RenderStar}
            />
          );
        });
    return null;
  };
  const settings = {
    dots: true,
    infinite:
      filterProduct(productData, {
        activeTab,
        filterCategories: [''],
      }).length > 3,
    speed: 500,
    slidesToShow: 4,
    slidesToScroll: 4,
    responsive: [
      {
        breakpoint: 739,
        settings: {
          slidesToShow: 2,
          slidesToScroll: 2,
          infinite: true,
          dots: true,
        },
      },
    ],
  };

  const renderTab = () => {
    const listTab = ['All', 'Furniture', 'Decoration', 'Lighting'];
    return listTab.map((value, index) => (
      <li
        key={index}
        onClick={(e) => handleClickTab(e)}
        data-name={value}
        className={activeTab === value ? 'active-li showing' : null}
      >
        {t(`product-category.${value.toUpperCase()}`)}
      </li>
    ));
  };

  return (
    <>
      <div className="trendy-product">
        <Element name="myScrollToTrendyProduct">
          <div className="grid wide">
            <h1 className="trendy-product__title">{t('trendy-product.title')}</h1>
            <ul className="trendy-product__menu">{renderTab()}</ul>
            {productData.length > 0 ? (
              <CSSTransition in={showing} classNames="product-animation" timeout={1000}>
                <div className="product-area">
                  <Slider style={{ marginLeft: '-12px' }} {...settings}>
                    {ListTrendyProducts()}
                  </Slider>
                </div>
              </CSSTransition>
            ) : (
              <Spinner />
            )}
          </div>
        </Element>
      </div>
    </>
  );
};

export default React.memo(TrendyProduct);
