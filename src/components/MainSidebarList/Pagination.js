import React, { useState, useEffect } from 'react';

function Pagination(props) {
  const { aclass, count, unit = 5, filters, cols, pos, setPos } = props;

  const pageCount = parseInt(count / unit) + (count % unit > 0 ? 1 : 0);
  const pageNumbers = [];

  for (let i = 0; i < pageCount; i++) {
    pageNumbers.push(i + 1);
  }

  function onPageLink(e, index) {
    setPos(index - 1);
  }

  function onPrev(e) {
    if (pos === 0) return;
    setPos(pos - 1);
  }

  function onNext(e) {
    if (pos === pageCount - 1) return;

    setPos(pos + 1);
  }

  return (
    <nav aria-label="Page navigation" style={{ display: count === 0 ? 'none' : '' }}>
      <ul className={`pagination ${aclass}`}>
        <li className={`page-item ${pos === 1 ? 'disabled' : ''}`}>
          <button
            className="page-link page-link-prev"
            to="#"
            aria-label="Previous"
            tabIndex="-1"
            aria-disabled="true"
            onClick={onPrev}
          >
            <span aria-hidden="true">
              <i className="icon-long-arrow-left" />
            </span>
            Prev
          </button>
        </li>

        {pageNumbers.map((item, index) => (
          <li
            className={`page-item ${item === pos ? 'active' : ''}`}
            aria-current="page"
            key={index}
          >
            <button className="page-link" to="#" onClick={(e) => onPageLink(e, item)}>
              {item}
            </button>
          </li>
        ))}

        {pageCount > 3 && pos < pageCount ? (
          <li className="page-item-total">of {pageCount}</li>
        ) : (
          ''
        )}

        <li className={`page-item ${pageCount === pos ? 'disabled' : ''}`}>
          <button className="page-link page-link-next" to="#" aria-label="Next" onClick={onNext}>
            Next{' '}
            <span aria-hidden="true">
              <i className="icon-long-arrow-right" />
            </span>
          </button>
        </li>
      </ul>
    </nav>
  );
}

export default Pagination;
