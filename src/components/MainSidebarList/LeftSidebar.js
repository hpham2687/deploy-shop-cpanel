import React from 'react';
import { Redirect } from 'react-router-dom';
import { CSSTransition, TransitionGroup } from 'react-transition-group';
import { getListCategory } from '../../helpers';

const LeftSideBar = (props) => {
  const { pos, setPos } = props;
  const filterCategoryOptions = props.filterOptions.category
    ? props.filterOptions.category
    : 'Decor';

  const data = props.productDataContext.productData;
  if (!data) return <Redirect to="/" />;

  const listCategory = getListCategory(data);
  return listCategory.map((el, index) => {
    return (
      <tr>
        <th>
          <div className="filter-checkbox">
            <input
              type="checkbox"
              className="custom-control-input"
              id={`id${el.name}`}
              onClick={(e) => {
                props.handleChangefilterCategoryOptions(e.target.name);
                setPos(0);
              }}
              name={el.name}
              checked={filterCategoryOptions.includes(el.name)}
            />
            <label className="custom-control-label" htmlFor={`id${el.name}`}>
              {el.name}
            </label>
          </div>
        </th>
        <th>
          <span className="category-amount">{el.count}</span>
        </th>
      </tr>
    );
  });
};

export default LeftSideBar;
