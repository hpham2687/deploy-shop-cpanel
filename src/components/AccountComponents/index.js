import React from 'react';
import { Formik } from 'formik';
import { showNotification } from '../commons/Notification';

const validate = (values) => {
  const errors = {};
  if (!values.firstName) {
    errors.firstName = 'Required';
  } else if (values.firstName.length < 3) {
    errors.firstName = 'Must be 3 characters or more';
  }

  if (!values.lastName) {
    errors.lastName = 'Required';
  } else if (values.lastName.length < 3) {
    errors.lastName = 'Must be 3 characters or more';
  }
  if (!values.displayName) {
    errors.displayName = 'Required';
  } else if (values.displayName.length < 10) {
    errors.displayName = 'Must be 10 characters or more';
  }

  if (values.currentPassword) {
    if (!values.currentPassword) {
      errors.currentPassword = 'Required';
    } else if (values.currentPassword.length < 6) {
      errors.currentPassword = 'Must be 6 characters or more';
    }

    if (!values.newPassword) {
      errors.newPassword = 'Required';
    } else if (values.newPassword.length < 6) {
      errors.newPassword = 'Must be 6 characters or more';
    }

    if (!values.confirmPassword) {
      errors.confirmPassword = 'Required';
    } else if (values.confirmPassword.length < 6) {
      errors.confirmPassword = 'Must be 6 characters or more';
    } else if (values.confirmPassword !== values.newPassword) {
      errors.confirmPassword = 'Not Match';
    }
  }

  return errors;
};

const MainAccount = (props) => {
  const { firebase, userData } = props;

  return (
    <div className="checkout-containter">
      <div className="page-title-banner">
        <h3 className="page-title-banner___title">My Account</h3>
        <span className="page-title-banner___subtitle">SHOP</span>
      </div>

      <div className="checkout-content-wrapper">
        <div className="grid wide">
          <div className="row">
            <div className="col l-3 m-6 c-12">
              <div className="checkout-leftmenu">
                <ul>
                  <li className="checkout-leftmenu--active">Account Details</li>
                  <li>Addresses</li>
                  <li>Orders</li>
                  <li>Sign Out</li>
                </ul>
              </div>
            </div>
            <div className="col l-9 m-6 c-12">
              <div className="checkout-right">
                <Formik
                  initialValues={{
                    firstName: userData ? userData.firstName : '',
                    lastName: userData ? userData.lastName : '',
                    displayName: userData ? userData.displayName : '',
                    currentPassword: '',
                    newPassword: '',
                    confirmPassword: '',
                  }}
                  validate={validate}
                  onSubmit={(values, { setSubmitting }) => {
                    const data2Update = { ...values };
                    delete data2Update.currentPassword;
                    delete data2Update.newPassword;
                    delete data2Update.confirmPassword;

                    firebase
                      .updateUserInfo(userData.uid, data2Update)
                      .then(function () {
                        showNotification('success', 'User info successfully updated!');
                      })
                      .catch(function (error) {
                        showNotification('success', 'Error updating User info!');
                      });

                    //  handleChangePassword();
                    if (values.currentPassword) {
                      firebase
                        .updateUserPassWord(values.newPassword)
                        .then(function () {
                          showNotification('success', 'change pass success!');
                        })
                        .catch(function (error) {
                          console.log(error);
                          showNotification('success', 'change pass fail!');
                        });
                    }
                    setSubmitting(false);
                  }}
                >
                  {({
                    values,
                    errors,
                    touched,
                    handleChange,
                    handleBlur,
                    handleSubmit,
                    isSubmitting,
                    /* and other goodies */
                  }) => (
                    <form onSubmit={handleSubmit}>
                      <div className="row">
                        <div className="col l-6 c-12">
                          <label className="account-label">First Name *</label>
                          <input
                            type="text"
                            className={`form-control-input ${
                              errors.firstName ? '--input-error' : null
                            }`}
                            id="firstName"
                            name="firstName"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.firstName}
                          />
                          <small className="form-text">
                            {errors.firstName && touched.firstName && errors.firstName}
                          </small>
                        </div>
                        <div className="col l-6 c-12">
                          <label className="account-label">Last Name *</label>
                          <input
                            type="text"
                            className={`form-control-input ${
                              errors.lastName ? '--input-error' : null
                            }`}
                            id="lastName"
                            name="lastName"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.lastName}
                          />
                          <small className="form-text">
                            {errors.lastName && touched.lastName && errors.lastName}
                          </small>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col l-12">
                          <label className="account-label">Display Name *</label>
                          <input
                            type="text"
                            className={`form-control-input ${
                              errors.displayName ? '--input-error' : null
                            }`}
                            id="displayName"
                            name="displayName"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.displayName}
                          />
                          <small className="form-text">
                            {errors.displayName && touched.displayName && errors.displayName}
                          </small>
                          <small className="form-text">
                            This will be how your name will be displayed in the account section and
                            in reviews
                          </small>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col l-12 c-12">
                          <label className="account-label">Email*</label>
                          <input
                            type="email"
                            className="form-control-input"
                            defaultValue={userData.email}
                            disabled
                          />
                        </div>
                      </div>

                      <div className="row">
                        <div className="col l-12 c-12">
                          <label className="account-label">
                            Current password (leave blank to leave unchanged)
                          </label>
                          <input
                            type="password"
                            className={`form-control-input ${
                              errors.currentPassword ? '--input-error' : null
                            }`}
                            id="currentPassword"
                            name="currentPassword"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.currentPassword}
                          />
                          <small className="form-text">
                            {errors.currentPassword &&
                              touched.currentPassword &&
                              errors.currentPassword}{' '}
                          </small>
                        </div>
                      </div>

                      <div className="row">
                        <div className="col l-12 c-12">
                          <label className="account-label">
                            New password (leave blank to leave unchanged)
                          </label>
                          <input
                            type="password"
                            className={`form-control-input ${
                              errors.newPassword ? '--input-error' : null
                            }`}
                            name="newPassword"
                            id="newPassword"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.newPassword}
                          />
                          <small className="form-text">
                            {errors.newPassword && touched.newPassword && errors.newPassword}
                          </small>
                        </div>
                      </div>
                      <div className="row">
                        <div className="col l-12 c-12">
                          <label className="account-label">Confirm new password</label>
                          <input
                            type="password"
                            className={`form-control-input ${
                              errors.confirmPassword ? '--input-error' : null
                            }`}
                            id="confirmPassword"
                            name="confirmPassword"
                            onBlur={handleBlur}
                            onChange={handleChange}
                            value={values.confirmPassword}
                          />
                          <small className="form-text">
                            {errors.confirmPassword &&
                              touched.confirmPassword &&
                              errors.confirmPassword}
                          </small>

                          <button
                            type="submit"
                            className="carousel__btn btn-outline-orange checkout-btn"
                            disabled={isSubmitting}
                            value="submit"
                          >
                            SAVE CHANGES
                            <i className="fa fa-caret-right" />
                          </button>
                        </div>
                      </div>
                    </form>
                  )}
                </Formik>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
  // else {
  //   props.history.push("/");
  //   showNotification("success", "Please go to account page by sidebar!");
  //   return null;
  // }
};

export default MainAccount;
