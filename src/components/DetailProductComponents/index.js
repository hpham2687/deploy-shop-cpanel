import React, { useState, useRef } from "react";
import { Animated } from "react-animated-css";
import Tabs from "./Tabs";
import { convert2SmallImg } from "../../utils";
import { showNotification } from "./../commons/Notification";

const MainDetailProduct = (props) => {
  let { productDataContext } = props;

  let [indexImg, setIndexImg] = useState(0);
  let [isShowAnimation, setIsShowAnimation] = useState(true);
  let [amount2Add, setAmount2Add] = useState(1);
  let [isShowModal, setIsShowModal] = useState(false);
  const imgDiv = useRef();

  let choosingProduct =
    productDataContext.productData.filter((value, index) => {
      return value.doc_id === props.idProduct;
    }) || 2;

  console.log(choosingProduct);
  if (choosingProduct.length === 0) {
    props.history.push("/");
    return null;
  }
  const imageList = choosingProduct[0].doc_data.pictures;
  const CategoryArray = choosingProduct[0].doc_data.category;

  const handleMouseMove = (e) => {
    const { left, top, width, height } = e.target.getBoundingClientRect();
    const x = ((e.pageX - left) / width) * 100;
    const y = ((e.pageY - top) / height) * 100;
    imgDiv.current.style.backgroundPosition = `${x}% ${y}%`;
  };

  const handleClickPrevious = () => {
    setIsShowAnimation(false);
    if (indexImg > 0) setIndexImg((prevState) => prevState - 1);
    if (indexImg === 0) setIndexImg(imageList.length - 1);
    setInterval(() => {
      setIsShowAnimation(true);
    }, 1000);
  };
  const handleClickFollowing = () => {
    setIsShowAnimation(false);
    if (indexImg < imageList.length - 1)
      setIndexImg((prevState) => prevState + 1);
    if (indexImg === imageList.length - 1) setIndexImg(0);
    if (indexImg === 0) setIndexImg(imageList.length - 1);
    setInterval(() => {
      setIsShowAnimation(true);
    }, 1000);
  };

  const renderModalShowFullScreenImg = () => {
    if (isShowModal)
      return (
        <>
          <div className="modal-show-fullscreen-img">
            <div className="modal-show-fullscreen-img__content">
              <div className="modal-show-fullscreen-img-center">
                <Animated
                  animationIn="bounceInLeft"
                  animationOut="bounceOutRight"
                  isVisible={isShowAnimation}
                >
                  <img
                    src={convert2SmallImg(imageList[indexImg])}
                    alt="big imgs"
                  />
                </Animated>
              </div>
              <div className="modal-show-fullscreen-img__toolbar">
                <i class="fas fa-search-plus"></i>
                <i class="fas fa-search-minus"></i>
                <i
                  onClick={() => setIsShowModal(false)}
                  class="fas fa-times"
                ></i>
              </div>
              <div className="modal-show-fullscreen-img__navigator">
                <i
                  onClick={handleClickPrevious}
                  class="fas fa-chevron-left"
                ></i>
                <i
                  onClick={handleClickFollowing}
                  class="fas fa-chevron-right"
                ></i>
              </div>
            </div>
            <div className="modal-show-fullscreen-img__overlay"></div>
          </div>
        </>
      );
    else return null;
  };
  const handleChangeAmount = (e) => {
    let typeChange = e.currentTarget.dataset.name;
    if (typeChange === "inc") setAmount2Add((prevState) => prevState + 1);
    else {
      setAmount2Add((prevState) => prevState - 1);
    }
  };
  const renderTabs = () => {
    return (
      <>
        <Tabs>
          <div className="description-tab" label="Description">
            <h3>Product Information</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec
              odio. Quisque volutpat mattis eros. Nullam malesuada erat ut
              turpis. Suspendisse urna viverra non, semper suscipit, posuere a,
              pede. Donec nec justo eget felis facilisis fermentum. Aliquam
              porttitor mauris sit amet orci. Aenean dignissim pellentesque
              felis. Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec
              consectetuer ligula vulputate sem tristique cursus.
            </p>
          </div>
          <div className="description-tab" label="Additional">
            <h3>Information</h3>
            <p>
              Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Donec
              odio. Quisque volutpat mattis eros. Nullam malesuada erat ut
              turpis. Suspendisse urna viverra non, semper suscipit, posuere a,
              pede. Donec nec justo eget felis facilisis fermentum. Aliquam
              porttitor mauris sit amet orci. Aenean dignissim pellentesque
              felis. Phasellus ultrices nulla quis nibh. Quisque a lectus. Donec
              consectetuer ligula vulputate sem tristique cursus. orem ipsum
              dolor sit amet, consectetuer adipiscing elit. Donec odio. Quisque
              volutpat mattis eros. Nullam malesuada erat ut turpis. Suspendisse
              urna viverra non, semper suscipi
            </p>
          </div>
          <div className="review-tab" label="Reviews">
            <div className="review-item">
              <div className="review-item__left">
                <div className="review-item__name tab-heading ">Samata Jr</div>
                <div class="star">
                  <i class="fa fa-star active-star"></i>
                  <i class="fa fa-star active-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                </div>
              </div>
              <div className="review-item__right">
                <div className="brief-rev tab-heading ">Good, perfect size</div>
                <div className="content-rev">
                  orem ipsum dolor sit amet, consectetur adipisicing elit.
                  Ducimus cum dolores assumenda asperiores facilis porro
                  reprehenderit animi culpa atque blanditiis commodi
                  perspiciatis doloremque, possimus, explicabo, autem fugit
                  beatae quae volup
                </div>
              </div>
            </div>
          </div>
        </Tabs>
      </>
    );
  };

  const handleAddToCart = ({ doc_id, doc_data }) => {
    props.cartContexts.cartDispatch({
      type: "SET",
      payload: { doc_id, doc_data },
      amount: amount2Add,
    });
  };
  return (
    <>
      {renderModalShowFullScreenImg()}

      <div class="detail-page">
        <div class="grid wide">
          <div class="row">
            <div class="col l-15">
              <div class="detail-page__small-preview">
                {imageList.map((value, index) => {
                  return (
                    <img
                      key={index}
                      src={convert2SmallImg(value)}
                      alt="product back"
                    />
                  );
                })}
              </div>
            </div>
            <div class="col l-5">
              <div
                class="detail-page__big-preview"
                ref={imgDiv}
                onMouseLeave={() =>
                  (imgDiv.current.style.backgroundPosition = `center`)
                }
                onMouseMove={handleMouseMove}
                style={{
                  backgroundImage: `url(${convert2SmallImg(
                    imageList[indexImg]
                  )})`,
                }}
              >
                <div
                  onClick={() => setIsShowModal(true)}
                  className="show-fullscreen-img-btn"
                >
                  <i class="fal fa-arrows-alt"></i>
                </div>
              </div>
            </div>
            <div class="col l-55">
              <div className="detail-content-container">
                <h3>Flow Slim Armchair</h3>
                <div class="star">
                  <i class="fa fa-star active-star"></i>
                  <i class="fa fa-star active-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <i class="fa fa-star"></i>
                  <span class="num-review">(2 Reviews)</span>
                </div>
                <span className="detail-price">$944</span>
                <p>
                  Sed ut perspiciatis, unde omnis iste natus error sit
                  voluptatem accusantium doloremque laudantium, totam rem
                  aperiam eaque ipsa, quae ab illo inventore veritatis et quasi
                  architecto beatae vitae dicta sunt, explicabo. Nemo enim ipsam
                  voluptatem,
                </p>
                <div class="quanity-group">
                  <span>Qty</span>
                  <div className="cart-product-quantity-btn --detail-page">
                    <div
                      onClick={handleChangeAmount}
                      data-name="dec"
                      className="input-group-prepend"
                    >
                      <button
                        className="btn btn-decrement btn-spinner"
                        type="button"
                        style={{ minWidth: "26px" }}
                      >
                        <i className="fas fa-minus" />
                      </button>
                    </div>
                    <input
                      type="text"
                      className="form-control "
                      required
                      value={amount2Add}
                      style={{ textAlign: "center" }}
                    />
                    <div
                      onClick={handleChangeAmount}
                      data-name="inc"
                      className="input-group-append"
                    >
                      <button
                        className="btn btn-increment btn-spinner"
                        type="button"
                      >
                        <i className="fas fa-plus" />
                      </button>
                    </div>
                  </div>
                </div>
                <div
                  onClick={() => handleAddToCart(choosingProduct[0])}
                  class="carousel__btn btn-outline-orange checkout-btn --checkout-btn-page "
                >
                  <i
                    style={{ marginRight: "10px" }}
                    class="far fa-cart-plus"
                  ></i>
                  ADD TO CART
                </div>
                <div className="detail-content-container__bottom">
                  <span>
                    Category:{" "}
                    {CategoryArray.map((val, i) => {
                      if (
                        CategoryArray.length > 1 &&
                        i !== CategoryArray.length - 1
                      ) {
                        return val + ", ";
                      } else {
                        return val;
                      }
                    })}
                  </span>
                  <div className="social-share-icons">
                    <span>Share:</span>
                    {["facebook-f", "twitter", "pinterest", "facebook"].map(
                      (value) => (
                        <a href="/" className="social-icon">
                          <i class={`fab fa-${value}`}></i>
                        </a>
                      )
                    )}
                  </div>
                </div>
              </div>
            </div>
          </div>
          <div className="row">
            <div className="col l-12">{renderTabs()}</div>
          </div>
        </div>
      </div>
    </>
  );
};

export default MainDetailProduct;
