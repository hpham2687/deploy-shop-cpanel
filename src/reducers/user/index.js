export const userReducer = (state, action) => {
  switch (action.type) {
    case "SET":
      return { ...state, authUser: action.payload };

    default:
      return state;
  }
};
