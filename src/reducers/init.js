export default {
  initialUserState: {
    authUser: null,
  },
  initialProductDataState: [],
  initialUiState: {
    isShowModalSidebar: false,
    isShowModalAuth: false,
    isShowModalResetPassword: false,
    isShowModalViewCart: false,
    trendyProductTab: 'All',
    recentArrivalProductTab: 'All',
    currency: 'usd',
  },

  initialCartState: [],
  initialWishlistState: 0,
};
