export const wishlistReducer = (state, action) => {
  switch (action.type) {
    case 'SET_NUM_WISHLIST':
      return action.payload;
    default:
      return state;
  }
};
