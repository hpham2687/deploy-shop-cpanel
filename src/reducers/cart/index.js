import { showNotification } from '../../components/commons/Notification';

export const cartReducer = (state, action) => {
  switch (action.type) {
    case 'SET':
      let isExist = false;
      const num2Add = action.amount || 1;
      const cloneState = [...state];
      cloneState.forEach((value, index) => {
        if (value.doc_id === action.payload.doc_id) {
          console.log('trung');
          isExist = true;
          action.payload.count = value.count + num2Add;
          cloneState.splice(index, 1); // xóa 1 phần tử từ vị trí 2
        }
      });
      if (!isExist) action.payload.count = num2Add;

      cloneState.push(action.payload);

      localStorage.setItem('cart', JSON.stringify(cloneState));
      showNotification(
        'success',
        'Item added to card! click vào biểu tượng giỏ hàng để sang page CHeckout'
      );
      return cloneState;
    case 'DELETE':
      const newState = [...state];
      var filtered = newState.filter((value) => value.doc_id !== action.payload);
      localStorage.setItem('cart', JSON.stringify(filtered));

      return filtered;
    default:
      return state;
  }
};
