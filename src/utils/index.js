import { showNotification } from '../components/commons/Notification';

export const isInWishlish = (doc_id) => {
  const storedWishList = JSON.parse(localStorage.getItem('wishList'));
  if (storedWishList) {
    return containsObject(doc_id, storedWishList);
  }
};
export const handleAddToWishList = ({ doc_id, doc_data }) => {
  let storedWishList = JSON.parse(localStorage.getItem('wishList'));
  if (!storedWishList) {
    localStorage.setItem('wishList', JSON.stringify([{ doc_id, doc_data }]));
  } else {
    // check exist
    if (containsObject(doc_id, storedWishList)) {
      storedWishList = storedWishList.filter((item) => item.doc_id !== doc_id);
      showNotification('success', 'Item removed from your wishlist!');
    } else {
      storedWishList.push({ doc_id, doc_data });
      showNotification('success', 'Item added to your wishlist!');
    }
    localStorage.setItem('wishList', JSON.stringify(storedWishList));
  }
  return countWishlish();

  // localStorage.getItem('wishList')
};
export const countWishlish = () => {
  return JSON.parse(localStorage.getItem('wishList')).length;
};
const containsObject = (doc_id, lists) => {
  let isExist = false;
  lists.forEach((item) => {
    if (item.doc_id === doc_id) {
      isExist = true;
    }
  });

  return isExist;
};

/**
 * utils to scroll element to top
 */
export const scrollTop = function () {
  const scrollTop = document.querySelector('#scroll-top');
  document.addEventListener('scroll', function () {
    if (window.pageYOffset >= 400) {
      scrollTop.classList.add('show');
    } else {
      scrollTop.classList.remove('show');
    }
  });

  scrollTop.addEventListener('click', function (e) {
    if (isIEBrowser() || isSafariBrowser() || isEdgeBrowser()) {
      let pos = window.pageYOffset;
      const timerId = setInterval(() => {
        if (pos <= 0) clearInterval(timerId);
        window.scrollBy(0, -120);
        pos -= 120;
      }, 1);
    } else {
      window.scrollTo({
        top: 0,
        behavior: 'smooth',
      });
    }
    e.preventDefault();
  });
};

/**
 * utils to detect IE browser
 * @return {bool}
 */
export const isIEBrowser = function () {
  const sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf('Trident') > -1) return true;
  return false;
};

/**
 * utils to detect safari browser
 * @return {bool}
 */
export const isSafariBrowser = function () {
  const sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf('Safari') !== -1 && sUsrAg.indexOf('Chrome') === -1) return true;
  return false;
};

/**
 * utils to detect Edge browser
 * @return {bool}
 */
export const isEdgeBrowser = function () {
  const sUsrAg = navigator.userAgent;
  if (sUsrAg.indexOf('Edge') > -1) return true;
  return false;
};

export const numberWithCommas = (x) => {
  return x.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ',');
};

export const convert2SmallImg = (url) => {
  if (!url.includes('http')) {
    url = `https://d-themes.com/react/molla/demo-2/${url}`;
  }

  return url;
};

// export const countStarProduct = (reviews, numReview) => {
//   return Math.floor(
//     reviews.reduce(function (acc, val) {
//       return acc + val.count || 2;
//     }, 0) / numReview
//   );
// };

export const filterProduct = (data, filterObject) => {
  const { activeTab } = filterObject;
  const isFilterActiveTab = activeTab !== null;
  return data
    .filter((value) => {
      // lọc qua từng phần tử
      if (!isFilterActiveTab) {
        // nếu có lọc qua active tab,
        return 1;
      }
      if (activeTab === 'All') return 1; // nếu tab là all, cho qua
      // check trong từng categrory của phần tử, nếu == với activeTab thì cho qua
      return value.doc_data.category.includes(activeTab);
    })
    .filter((value) => {
      // lọc trendy

      if (!filterObject.filterCategories.includes('trendy')) {
        return 1;
      }
      if (value.doc_data.top) return 1;
      return 0;
    })
    .filter((value) => {
      // lọc theo time
      if (!filterObject.filterCategories.includes('newArrival')) {
        return 1;
      }
      if (!value.doc_data.new) return 1;
      return 0;
    });
};

/**
 * Get trendy products
 * @param {Array} products
 * @return {Array} trendyProducts
 */
export const getTrendyProducts = (products) => {
  return products.filter((item) => item.trendy);
};

/**
 * Get new products
 * @param {Array} products
 * @return {Array} newProducts
 */
export const getNewProducts = (products) => {
  return products.filter((item) => item.new);
};
