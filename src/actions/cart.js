export const cartActionTypes = {
  SET: "SET",
  DELETE: "DELETE",
};

export const setCart = (payload) => {
  return {
    type: cartActionTypes.SET,
    payload,
  };
};

export const loginUser = (user, remember) => (dispatch) => {
  return dispatch({ type: cartActionTypes.LOGIN, user, remember });
};

export const deleteCart = () => (dispatch) => {
  return dispatch({ type: cartActionTypes.REMOVE });
};
