export const cartActionTypes = {
  SET_MODAL_AUTH_STATUS: 'SET_MODAL_AUTH_STATUS',
  SET_MODAL_RESET_PASSWORD: 'SET_MODAL_RESET_PASSWORD',

  SET_MODAL_VIEW_CART_STATUS: 'SET_MODAL_VIEW_CART_STATUS',
  SET_MODAL_SIDEBAR: 'SET_MODAL_SIDEBAR',
  SET_TRENDY_PRODUCT_TAB: 'SET_TRENDY_PRODUCT_TAB',
  SET_RECENT_ARRIVAL_PRODUCT_TAB: 'SET_RECENT_ARRIVAL_PRODUCT_TAB',
  SET_CURRENCY: 'SET_CURRENCY',
};

export const setModalResetPassword = (payload) => {
  return {
    type: cartActionTypes.SET_MODAL_RESET_PASSWORD,
    payload,
  };
};

export const setModalAuthStatus = (payload) => {
  return {
    type: cartActionTypes.SET_MODAL_AUTH_STATUS,
    payload,
  };
};
export const setModalViewCartStatus = (payload) => {
  return {
    type: cartActionTypes.SET_MODAL_VIEW_CART_STATUS,
    payload,
  };
};
export const setModalwModalSidebar = (payload) => {
  return {
    type: cartActionTypes.SET_MODAL_SIDEBAR,
    payload,
  };
};

export const setTrendyProductTab = (payload) => {
  return {
    type: cartActionTypes.SET_TRENDY_PRODUCT_TAB,
    payload,
  };
};
export const setRecentArrivalProductTab = (payload) => {
  return {
    type: cartActionTypes.SET_RECENT_ARRIVAL_PRODUCT_TAB,
    payload,
  };
};
export const setCurrency = (payload) => {
  return {
    type: cartActionTypes.SET_CURRENCY,
    payload,
  };
};
